import { Injectable } from "@angular/core";
import { AngularDataContext } from "@themost/angular";

export declare interface ApiServerStatus {
  version: string;
  database: string;
  modifiedAt?: Date;
  attachedAt?: Date;
}

/**
 *
 * This Service is used to get diagnostics from the api server
 * @export
 * @class DiagnosticsService
 */

@Injectable()
export class DiagnosticsService {

  constructor(private context: AngularDataContext) { }

  /**
   *
   * Get status of api server
   * @returns {Promise<ApiServerStatus>} - Returns ApiServerStatus promise with version, database and optional modifiedAt/attachedAt dates.
   * @memberof DiagnosticsService
   */
  getStatus(): Promise<ApiServerStatus> {
    return this.context.getService().execute({
      method: 'GET',
      url: 'diagnostics/status',
      headers: { },
      data: null
    });
  }
}
