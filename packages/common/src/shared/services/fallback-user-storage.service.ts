import {AngularDataContext} from '@themost/angular';
import {UserStorageService} from './user-storage';
import {Inject, Injectable, InjectionToken, Optional} from '@angular/core';
import {LocalUserStorageService, SessionUserStorageService} from './browser-storage.service';

export declare interface UserStorageInterface {
  localStorage: boolean;
  sessionStorage: boolean;
}

export const USER_STORAGE = new InjectionToken<UserStorageInterface>('user-storage.location');


@Injectable()
export class FallbackUserStorageService {

  private _useSessionStorage = true;
  private _useLocalStorage = false;
  private _hasUserStorage: boolean;
  private _userStorageService: UserStorageService;
  private _localUserStorage: LocalUserStorageService;
  private _sessionUserStorage: SessionUserStorageService;

  constructor(private _context: AngularDataContext,
              @Optional() @Inject(USER_STORAGE) private userStorage?: UserStorageInterface) {
    this._userStorageService = new UserStorageService(_context);
    this._userStorageService.hasUserStorage().then((res) => {
      this._hasUserStorage = res;
      if (!this._hasUserStorage && userStorage) {
        if (userStorage.sessionStorage !== undefined) {
          this.userSessionStorage = userStorage.sessionStorage;
          this._sessionUserStorage = this.userSessionStorage ? new SessionUserStorageService(_context) : undefined;
        }
        if (userStorage.localStorage !== undefined) {
          this.userLocalStorage = userStorage.localStorage;
          this._localUserStorage = this.userLocalStorage ? new LocalUserStorageService(_context) : undefined;
        }
      }
    });
  }

  get userLocalStorage(): boolean {
    return this._useLocalStorage;
  }

  set userLocalStorage(useLocal: boolean) {
    this._useLocalStorage = useLocal;
  }

  get userSessionStorage(): boolean {
    return this._useSessionStorage;
  }

  set userSessionStorage(useSession: boolean) {
    this._useSessionStorage = useSession;
  }

  async getItem(key: string): Promise<any> {
    const userStorage = await this._userStorageService.hasUserStorage();
    if (userStorage) {
      return this._userStorageService.getItem(key);
    } else {
      if (this.userSessionStorage) {
        return this._sessionUserStorage.getItem(key);
      } else if (this.userLocalStorage) {
        return this._localUserStorage.getItem(key);
      } else {
        throw new Error('No user storage provider was found');
      }
    }
  }

  async setItem(key: string, value: string): Promise<any> {
    const userStorage = await this._userStorageService.hasUserStorage();
    if (userStorage) {
      return this._userStorageService.setItem(key, value);
    } else {
      if (this.userSessionStorage) {
        return this._sessionUserStorage.setItem(key, value);
      } else if (this.userLocalStorage) {
        return this._localUserStorage.setItem(key, value);
      } else {
        throw new Error('No user storage provider was found');
      }
    }
  }

  async removeItem(key: string): Promise<any> {
    const userStorage = await this._userStorageService.hasUserStorage();
    if (userStorage) {
      return this._userStorageService.removeItem(key);
    } else {
      if (this.userSessionStorage) {
        return this._sessionUserStorage.removeItem(key);
      } else if (this.userLocalStorage) {
        return this._localUserStorage.removeItem(key);
      } else {
        throw new Error('No user storage provider was found');
      }
    }
  }
}
