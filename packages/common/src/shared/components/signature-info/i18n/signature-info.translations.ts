import {en} from './signature-info.en';
import {el} from './signature-info.el';

export const SIGNATURE_INFO = {
  en: en,
  el: el
};
