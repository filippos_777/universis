import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.interfaces';

export class ScholarshipResultsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./scholarship-results-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`packages/registrar/src/app/scholarships/components/scholarships-preview/scholarship-preview-results/scholarship-results-table.config.list.json`);
        });
    }
}

export class ScholarshipResultsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./scholarship-results-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`packages/registrar/src/app/scholarships/components/scholarships-preview/scholarship-preview-results/scholarship-results-table.search.list.json`);
            });
    }
}

export class ScholarshipResultsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`packages/registrar/src/app/scholarships/components/scholarships-preview/scholarship-preview-results/scholarship-results-table.config.list.json`);
    }
}
