import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-overview-sections',
  templateUrl: './classes-overview-sections.component.html',
  styleUrls: ['./classes-overview-sections.component.scss']
})
export class ClassesOverviewSectionsComponent implements OnInit, OnDestroy  {

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  public class;
  private subscription: Subscription;

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.class = await this._context.model('CourseClasses')
        .where('id').equal(params.id)
        .expand('sections($expand=instructors($expand=instructor))')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
