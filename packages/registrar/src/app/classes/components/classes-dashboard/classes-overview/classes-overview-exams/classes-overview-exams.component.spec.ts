import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesOverviewExamsComponent } from './classes-overview-exams.component';

describe('ClassesOverviewExamsComponent', () => {
  let component: ClassesOverviewExamsComponent;
  let fixture: ComponentFixture<ClassesOverviewExamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesOverviewExamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesOverviewExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
