import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {AppEventService} from '@universis/common';
import {ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {Observable, Subscription} from 'rxjs';
import {AdvancedFormComponent} from '@universis/forms';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-copy-program',
  templateUrl: './copy-program.component.html'
})
export class CopyProgramComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  public loading = false;
  public lastError;
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  formConfig: any;
  private subscription: Subscription;
  private formChangeSubscription: Subscription;
  @Input() formEditName = 'StudyPrograms/copy';
  @Input() toastHeader = 'StudyPrograms.CopyProgram';



  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _translateService: TranslateService,
              private _context: AngularDataContext) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }

  ngOnInit() {
    this.formComponent.formName = this.formEditName;
    this.subscription = this.activatedRoute.data.subscribe((routeData) => {
      this.formComponent.data = {id: routeData.data.id};
    });
      this.formChangeSubscription = this.formComponent.form.change.subscribe((event) => {
      if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
        // enable or disable button based on form status
        this.okButtonDisabled = !event.isValid;
      }
    });

  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    return super.close({
      fragment: 'reload',
      skipLocationChange: true
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  async ok() {
    try {
      this.loading = true;
      this.lastError = null;
      const data = this.formComponent.form.formio.data;
      // create snapshot

      if (this.formComponent.data) {
        await this._context.model(`StudyPrograms/${data.id}/copy`).save(data);
        // and hide
        this.loading = false;
        this._toastService.show(
          this._translateService.instant('StudyPrograms.CopyProgram'),
          this._translateService.instant('StudyPrograms.CopyOperationStarted')
        );
        this._appEvent.change.next({model: 'StudyPrograms'});
        // close
        return super.close({
          fragment: 'reload',
          skipLocationChange: true
        });
      } else {
        this.loading = false;
        // close
        return super.close({
          fragment: 'reload',
          skipLocationChange: true
        });
      }
    } catch (err) {
      this.loading = false;
      this.lastError = err;
    }
  }

  onChange($event: any) {

  }

}
