import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as GROUP_COURSES_LIST_CONFIG from './group-courses-table.config.json';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: 'app-groups-courses-general',
  templateUrl: './group-courses-general.component.html'
})

export class GroupCoursesGeneralComponent implements OnInit {

  studyProgram: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  public studyProgramsId;
  public programGroup;
  public programGroups;
  public programGroupId;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
  ) { }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramsId = this._activatedRoute.snapshot.parent.params.id;
      this.programGroupId = params.id;

      this.programGroup = await this._context.model(`ProgramGroups`)
        .asQueryable()
        .where('program').equal(this.studyProgramsId)
        .and('id').equal(this.programGroupId)
        .expand('groupType,parentGroup,gradeScale,program($expand=department,studyLevel)')
        .getItem();
    });

  }

}
