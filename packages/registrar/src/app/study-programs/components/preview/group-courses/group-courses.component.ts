import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as GROUP_COURSES_LIST_CONFIG from './group-courses-table.config.json';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import {ActivatedTableService} from '../../../../tables/tables.activated-table.service';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';



@Component({
  selector: 'app-groups',
  templateUrl: './group-courses.component.html'
})

export class GroupCoursesComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration> GROUP_COURSES_LIST_CONFIG;
  @ViewChild('groupCourses') groupCourses: AdvancedTableComponent;
  studyProgram: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  public studyProgramsId;
  public programGroup;
  public programGroups;
  public programGroupId;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
  ) { }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramsId = this._activatedRoute.snapshot.parent.params.id;
      this.programGroupId = params.id;

      this.programGroups = await this._context.model(`ProgramGroups`)
        .asQueryable()
        .where('program').equal(this.studyProgramsId)
        .and('groupType').equal(2)
        .expand('groupType,parentGroup,gradeScale,program($expand=department,studyLevel)')
        .getItems();

    });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
