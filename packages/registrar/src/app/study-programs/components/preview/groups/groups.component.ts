import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as GROUPS_LIST_CONFIG from './groups-table.config.json';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import {ActivatedTableService} from '../../../../tables/tables.activated-table.service';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';


@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html'
})

export class GroupsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration> GROUPS_LIST_CONFIG;
  @ViewChild('groups') groups: AdvancedTableComponent;
  studyProgram: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
  ) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this._activatedTable.activeTable = this.groups;
      this.groups.query = this._context.model('ProgramGroups')
        .where('program').equal(params.id)
        // .and('groupType').equal(2)
        .expand('groupType,parentGroup')
        .prepare();
      this.groups.config = AdvancedTableConfiguration.cast(GROUPS_LIST_CONFIG);
      this.groups.fetch();
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.groups.fetch(true);
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.groups && this.groups.selected && this.groups.selected.length) {
      // get items to remove
      const items = this.groups.selected.map(item => {
        return {
          id: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('StudyPrograms.RemoveGroupTitle'),
        this._translateService.instant('StudyPrograms.RemoveGroupMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._context.model('programGroups').remove(items).then(() => {
            this._toastService.show(
              this._translateService.instant('StudyPrograms.RemoveGroupsMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'StudyPrograms.RemoveGroupsMessage.one' : 'StudyPrograms.RemoveGroupsMessage.many')
                , { value: items.length })
            );
            this.groups.fetch(true);
          }).catch(err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }
}
