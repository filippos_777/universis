import {ModuleWithProviders, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule,TranslateService} from '@ngx-translate/core';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {StudyProgramsPreviewFormComponent} from './components/preview/preview-general/study-programs-preview-form.component';
import { ProgramCoursePreviewFormComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-form.component';
import { StudyProgramsTableConfigurationResolver, StudyProgramsTableSearchResolver, StudyProgramsDefaultTableConfigurationResolver } from './components/study-programs-table/study-programs-table-config.resolver';
import { StudyProgramsPreviewCoursesConfigurationResolver, StudyProgramsPreviewCoursesSearchResolver, StudyProgramsDefaultPreviewCoursesConfigurationResolver } from './components/preview/preview-courses/study-programs-preview-courses-config.resolver';
import { EditCoursesComponent } from './components/preview/edit-courses/edit-courses.component';
import {MostModule} from '@themost/angular';
import {RouterModalModule} from '@universis/common/routing';
import {AdvancedFormsModule} from '@universis/forms';
import {SettingsSharedModule} from '../settings-shared/settings-shared.module';
import {CoursesSharedModule} from '../courses/courses.shared';
import {AddCoursesComponent} from './components/preview/add-courses/add-courses.component';
import {SpecializationCourseProgramCourseResolver} from './services/program-course.resolver';
import {AddGroupCoursesComponent} from './components/preview/group-courses/add-group-courses/add-group-courses.component';
// tslint:disable-next-line:max-line-length
import {GroupCoursesDetailsPercentComponent} from './components/preview/group-courses/group-courses-details/group-courses-details-percent/group-courses-details-percent.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    MostModule,
    RouterModalModule,
    AdvancedFormsModule,
    SettingsSharedModule,
      CoursesSharedModule
  ],
  declarations: [
    StudyProgramsPreviewFormComponent,
    ProgramCoursePreviewFormComponent,
    EditCoursesComponent,
    AddCoursesComponent,
    AddGroupCoursesComponent,
    GroupCoursesDetailsPercentComponent

  ],
  entryComponents: [
    EditCoursesComponent,
    AddCoursesComponent,
    AddGroupCoursesComponent,
    GroupCoursesDetailsPercentComponent
  ],
  exports: [
    StudyProgramsPreviewFormComponent,
    ProgramCoursePreviewFormComponent,
    EditCoursesComponent
  ],
  providers: [
  ]
})
export class StudyProgramsSharedModule implements OnInit {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StudyProgramsSharedModule,
      providers: [
        StudyProgramsTableConfigurationResolver,
        StudyProgramsTableSearchResolver,
        StudyProgramsDefaultTableConfigurationResolver,
        StudyProgramsPreviewCoursesConfigurationResolver,
        StudyProgramsPreviewCoursesSearchResolver,
        StudyProgramsDefaultPreviewCoursesConfigurationResolver,
        SpecializationCourseProgramCourseResolver
      ]
    };
  }

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading study programs shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/study-programs.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}
