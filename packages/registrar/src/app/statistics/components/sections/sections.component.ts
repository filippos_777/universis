import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ErrorService, LoadingService } from '@universis/common';
import { Observable, Subject, Subscription } from 'rxjs';
import { ReportService } from '../../../reports-shared/services/report.service';
import { StatisticsService } from '../../services/statistics-service/statistics.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';

@Component({
  selector: 'app-statistics-sections',
  templateUrl: './sections.component.html'
})
export class SectionsComponent implements OnInit, OnDestroy {

  @ViewChild('variablesModal') variablesModal;

  public category: string;
  public reportCategories: any[];
  public reportTemplates: any[];
  public formConfig;
  public search: string = null;
  public modal: BsModalRef;
  public lastError;
  public selectedReport; // the report to print
  public reportForm;
  public formProperties;
  public modalButtonsSubject: Subject<string>;
  public modalButtons$: Observable<string>;
  public okButtonDisabled = false;

  private _activatedRouteSubscription: Subscription;

  constructor(
    private readonly _statisticsService: StatisticsService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _loadingService: LoadingService,
    private readonly _errorService: ErrorService,
    private readonly _reportsService: ReportService,
    private readonly _modalService: BsModalService,
    private readonly _activeDepartmentService: ActiveDepartmentService
  ) { }

  async ngOnInit() {
    this._activatedRouteSubscription = this._activatedRoute.params.subscribe(async data => {
      await this.handleRouterChange(data);
    });
    this.modalButtonsSubject = new Subject();
    this.modalButtons$ = this.modalButtonsSubject.asObservable();
  }

  async handleRouterChange(data): Promise<void> {
    if (data && data.category) {
      this.category = data.category;
    }
    await this.fetchData();
  }

  async fetchData(): Promise<void> {
    this.reportTemplates = undefined;
    try {
      this._loadingService.showLoading();
      this.reportTemplates = await this._statisticsService.getReportTemplatesOfCategory(this.category);
    } catch (err) {
      this._errorService.setLastError(err);
      this._errorService.showError(err);
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  async handleReportPrint(id: number) {
    try {
      this.closeModal();
      this._loadingService.showLoading();
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment()
      this.formProperties = {
        ID: activeDepartment.id
      };
      const { inputControls } = await this._reportsService.getReportData(id);
      const report = await this._reportsService.getReport(id);
      if (inputControls) {
        this.formConfig = this._reportsService.generateVariablesForm(inputControls);
      }
      this.selectedReport = report;
      this.reportForm = await this._reportsService.getReportFormFor(report);

      this.modal = this._modalService.show(this.variablesModal, {
        class: 'modal-xl',
        ignoreBackdropClick: true,
        focus: true,
        keyboard: false
      });
      if (window && window.document) {
        window.document.body.style.overflowY = "hidden";
      }
    } catch(err) {
      console.error(err);
      this._errorService.showError(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  onFormValidation(event: {isValid: boolean, formName: string}) {
    // handle changes (check if event has isValid property)
    if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
      // enable or disable button based on form status
      this.okButtonDisabled = !event.isValid;
    }
  }

  closeModal() {
    if(this.modal && this.modal.hide) {
      this.modal.hide();
      window.document.body.style.overflowY = "auto";
    }
  }

  ok() {
    this.modalButtonsSubject.next("submit");
  }

  ngOnDestroy() {
    if (this._activatedRouteSubscription && !this._activatedRouteSubscription.closed) {
      this._activatedRouteSubscription.unsubscribe();
    };
  }
}
