import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '../../../../tables/components/advanced-table/advanced-table.component';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {Subscription} from 'rxjs';
import * as THESES_DASHBOARD_STUDENTS_LIST_CONFIG from './theses-dashboard-students.config.list.json';
import {ActivatedTableService} from '../../../../tables/tables.activated-table.service';
import * as CLASSES_INSTRUCTORS_LIST_CONFIG
  from "../../../../classes/components/classes-dashboard/classes-instructors/classes-instructors.config.list.json";
import * as REGISTRATIONS_LIST_CONFIG
  from "../../../../registrations/components/registrations-table/registrations-table.config.list.json";
import {TableConfiguration} from "../../../../tables/components/advanced-table/advanced-table.interfaces";



@Component({
  selector: 'app-theses-dashboard-students',
  templateUrl: './theses-dashboard-students.component.html',
})
export class ThesesDashboardStudentsComponent implements OnInit, OnDestroy {

  public model: any;
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>THESES_DASHBOARD_STUDENTS_LIST_CONFIG;
  private dataSubscription: Subscription;
  private subscription: Subscription;
  @ViewChild('students') students: AdvancedTableComponent;
  public thesesID: any;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext) {}

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this._activatedTable.activeTable = this.students;
      this.thesesID = params.id;
      this.students.query = this._context.model('Theses/' + this.thesesID + '/students')
        .asQueryable()
        .expand('student($expand=department,person,studentStatus)')
        .prepare();

      this.students.config = AdvancedTableConfiguration.cast(THESES_DASHBOARD_STUDENTS_LIST_CONFIG);
      this.students.fetch();
  
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe( fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });
      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.students && this.students.selected && this.students.selected.length) {
      const items = this.students.selected.map( student => {
        return {
          thesis: this.thesesID,
          student: student
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Theses.RemoveStudentTitle'),
        this._translateService.instant('Theses.RemoveStudentMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          this._context.model('StudentTheses').remove(items).then( () => {
            this._toastService.show(
              this._translateService.instant('Theses.RemoveStudentsMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'Theses.RemoveStudentsMessage.one' : 'Theses.RemoveStudentsMessage.many')
                , { value: items.length })
            );
            this.students.fetch(true);
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });

    }
  }
}

