import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '../../../../../tables/components/advanced-table/advanced-table.component';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {ActivatedTableService} from '../../../../../tables/tables.activated-table.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-theses-dashboard-overview-grades',
  templateUrl: './theses-dashboard-overview-grades.component.html',
})
export class ThesesDashboardOverviewGradesComponent implements OnInit, OnDestroy {

  public model: any;
  public thesesID: any = this._activatedRoute.snapshot.params.id;
  private subscription: Subscription;
  // public grades: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {}

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {

      this.model = await this._context.model('Theses')
        .where('id').equal(params.id)
        .expand('instructor,type,status,students($expand=student($expand=department,person))')
        .getItem();
      this.thesesID = params.id;

      const grades = await this._context.model('StudentThesisResults')
        .where('thesis').equal(params.id)
        .expand('instructor')
        .getItems();

      this.model.students.forEach( student => {
        student.results = grades.filter( x => {
          return x.student === student.student.id;
        });
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
