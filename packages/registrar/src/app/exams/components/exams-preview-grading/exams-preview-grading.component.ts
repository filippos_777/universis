import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration,
  AdvancedTableDataResult } from '../../../tables/components/advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import * as EXAMS_ACTIONS_LIST_CONFIG from './exams-grading-table.config.json';
import { ErrorService, ModalService, ToastService } from '@universis/common';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedTableService } from '../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-exams-preview-grading',
  templateUrl: './exams-preview-grading.component.html',
  styleUrls: ['./exams-preview-grading.component.scss']
})

export class ExamsPreviewGradingComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>EXAMS_ACTIONS_LIST_CONFIG;
  @ViewChild('actions') actions: AdvancedTableComponent;
  courseExamID: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
  ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this._activatedTable.activeTable = this.actions;
      this.actions.query = this._context.model(`CourseExams/${params.id}/actions`)
        .asQueryable()
        .where('actionStatus/alternateName').equal('CompletedActionStatus')
        .or('actionStatus/alternateName').equal('FailedActionStatus')
        .or('actionStatus/alternateName').equal('ActiveActionStatus')
        .prepare().and('additionalResult').notEqual(null)
         .expand('owner,grades($select=action,count(id) as totalCount;$groupby=action)')
        .orderBy('dateCreated')
        .prepare();

      this.actions.config = AdvancedTableConfiguration.cast(EXAMS_ACTIONS_LIST_CONFIG);
      this.actions.fetch();

    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
