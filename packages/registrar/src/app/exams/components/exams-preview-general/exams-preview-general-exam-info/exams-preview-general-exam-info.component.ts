import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {combineLatest, Subscription} from 'rxjs';
import {AppEventService} from '@universis/common';

@Component({
  selector: 'app-exams-preview-general-exam-info',
  templateUrl: './exams-preview-general-exam-info.component.html',
  styleUrls: ['./exams-preview-general-exam-info.component.scss']
})
export class ExamsPreviewGeneralExamInfoComponent implements OnInit, OnDestroy  {

  public courseExam: any;
  public uploadGrades: any;
  public examId: any;
  private subscription: Subscription;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _appEvent: AppEventService) { }

  async ngOnInit() {
    this.subscription = combineLatest([this._activatedRoute.params, this._appEvent.change]).subscribe(async (params) => {
      this.examId = params[0].id;
      const eventChanges = params[1];
      this.courseExam = eventChanges && eventChanges.target && eventChanges.model === 'CourseExams' ? eventChanges.target
        : await this._context.model('CourseExams')
        .where('id').equal(params[0].id)
        .expand('course,examPeriod,status,completedByUser,year')
        .getItem();
    });

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
