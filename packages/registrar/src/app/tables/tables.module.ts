import {NgModule, OnInit, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvancedTableComponent, COLUMN_FORMATTERS} from './components/advanced-table/advanced-table.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {AdvancedTableSearchComponent} from './components/advanced-table/advanced-table-search.component';
import { DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {BtnExpDirective} from './directives/btnexp.directive';
import {AdvancedTableSettingsComponent} from './components/advanced-table/advanced-table-settings.component';
import {SharedModule, TemplatePipe} from '@universis/common';
import { AdvancedTableModalBaseComponent } from './components/advanced-table-modal/advanced-table-modal-base.component';
import {RouterModalModule} from '@universis/common/routing';
import { AdvancedSearchFormComponent } from './components/advanced-search-form/advanced-search-form.component';
import {AdvancedFormsModule} from '@universis/forms';
import {FormioModule} from 'angular-formio';
import { AdvancedListComponent } from './components/advanced-list/advanced-list.component';
import {RouterModule} from '@angular/router';
import {
    AdvancedSearchConfigurationResolver,
    AdvancedTableConfigurationResolver
} from './components/advanced-table/advanced-table-resolvers';
import {AdvancedRowActionComponent} from './components/advanced-row-action/advanced-row-action.component';
import {FORMATTERS} from './components/advanced-table/advanced-table.formatters.interface';
import { AdvancedSelectService } from './components/advanced-select/advanced-select.service';
import { AdvancedSelectComponent } from './components/advanced-select/advanced-select.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AdvancedAggregatorComponent } from './components/advanced-aggregator/advanced-aggregator.component';
import { AdvancedAggregatorListComponent } from './components/advanced-aggregator-list/advanced-aggregator-list.component';
import { AdvancedAggregatorService } from './services/advanced-aggregator/advanced-aggregator.service';
import { NgPipesModule } from 'ngx-pipes';
import { GroupByPipe } from 'ngx-pipes';
import { ParseTitlePipe } from './pipes/advansed-aggregator-list-title-parse.pipe';
import { AdvancedAggregatorPipe } from './pipes/advanced-aggregator.pipe';
import {AddItemsComponent} from './components/add-items/add-items.component';
import {AdvancedTableEditorDirective} from './directives/advanced-table-editor.directive';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        RouterModalModule,
        FormioModule,
        AdvancedFormsModule,
        BsDropdownModule,
        NgPipesModule
    ],
    providers: [DatePipe, TemplatePipe,
      {
        provide: COLUMN_FORMATTERS,
        useValue: FORMATTERS
      },
      GroupByPipe,
      AdvancedTableConfigurationResolver,
        AdvancedSearchConfigurationResolver,
        AdvancedSelectService,
        AdvancedAggregatorService
      ],
    declarations: [
        AdvancedTableComponent,
        AdvancedTableSearchComponent,
        BtnExpDirective,
        AdvancedTableSettingsComponent,
        AdvancedTableModalBaseComponent,
        AdvancedSearchFormComponent,
        AdvancedListComponent,
        AdvancedRowActionComponent,
        AdvancedSelectComponent,
        AdvancedAggregatorComponent,
        AdvancedAggregatorListComponent,
        ParseTitlePipe,
        AdvancedAggregatorPipe,
        AddItemsComponent,
        AdvancedTableEditorDirective
    ],
    exports: [
        AdvancedTableComponent,
        AdvancedTableSearchComponent,
        BtnExpDirective,
        AdvancedTableSettingsComponent,
        AdvancedSearchFormComponent,
        AdvancedListComponent,
        AdvancedRowActionComponent,
        AdvancedSelectComponent,
        AdvancedAggregatorComponent,
        AdvancedAggregatorListComponent,
        AddItemsComponent,
        AdvancedTableEditorDirective
    ],
    entryComponents: [
        AdvancedTableSettingsComponent,
        AdvancedRowActionComponent,
        AdvancedSelectComponent,
        AddItemsComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TablesModule implements OnInit {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.error('An error occurred while loading tables module');
            console.error(err);
        });
    }

    async ngOnInit() {
        environment.languages.forEach(language => {
            import(`./i18n/tables.${language}.json`).then((translations) => {
                this._translateService.setTranslation(language, translations, true);
            });
        });
    }

}
