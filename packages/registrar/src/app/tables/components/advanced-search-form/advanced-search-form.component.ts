import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormioComponent, FormioForm} from 'angular-formio';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {ServiceUrlPreProcessor, TranslatePreProcessor} from '@universis/forms';
import {AdvancedTableSearchBaseComponent} from '../advanced-table/advanced-table-search-base';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {AdvancedTableComponent} from '../advanced-table/advanced-table.component';

@Component({
  selector: 'app-advanced-search-form',
  template: `
    <div class="p-4">
      <formio #formioComponent class="formio-search" (keydown)="onKeyEnter($event)" (change)="onChange($event)" (formLoad)="onFormLoad($event)"
              [refresh]="refreshForm" [submission]="submission" [form]="form"></formio>
    </div>
  `,
  styleUrls: ['./advanced-search-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AdvancedSearchFormComponent extends AdvancedTableSearchBaseComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('formioComponent') formComponent: FormioComponent;
  @Input() form: any = {};
  @Output() refreshForm = new EventEmitter<any>();
  submission: any = { data: {} };
  private filterSubscription: Subscription;
  public loading = true;

  constructor(private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    new ServiceUrlPreProcessor(this._context).parse(this.form);
    new TranslatePreProcessor(this._translateService).parse(this.form);

    this.submission = {
      data: this.filter
    };
    this.refreshForm.emit({
      form: this.form
    });
    this.loading = false;
    this.filterSubscription = this.filterChange.subscribe( filter => {
      this.submission.data = filter;
      this.refreshForm.emit({
        submission: this.submission
      });
    });
  }

  onChange(event: object) {
    Object.keys(this.formComponent.submission.data).forEach(key => {
      if (Object.prototype.hasOwnProperty.call(this.formComponent.submission.data, key)) {
        const value = this.formComponent.submission.data[key];
        // the filter with type is a string and is not empty
        const isEmptyString =  typeof value === 'string' && value.length === 0;
        // do not include empty objects in filter
        const isEmptyObject = typeof value === 'object' && Object.keys(value).length === 0;
        if (!isEmptyString && !isEmptyObject) {
          Object.defineProperty(this.filter, key, {
            configurable : true,
            enumerable: true,
            value : value
          });
        } else {
          delete this.filter[key];
        }
      }
    });
  }

  onFormLoad(form: FormioForm) {
    const currentLang = this._translateService.currentLang;
    this.formComponent.formio.i18next.options.resources[currentLang] = { translation : this._translateService.instant(`Forms`) };
    this.formComponent.formio.language = currentLang;
  }

  ngAfterViewInit(): void {
    //
  }

  ngOnDestroy(): void {
    if (this.filterSubscription) {
      this.filterSubscription.unsubscribe();
    }
  }
}
