import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import * as INSTRUCTORS_STUDENTS_LIST_CONFIG from './instructors-dashboard-students.config.list.json';
import {Subscription} from 'rxjs';
import {ErrorService, ModalService, ToastService} from '@universis/common';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../tables/components/advanced-table/advanced-table.component';
import {AdvancedSearchFormComponent} from '../../../tables/components/advanced-search-form/advanced-search-form.component';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';


@Component({
  selector: 'app-instructors-dashboard-students',
  templateUrl: './instructors-dashboard-students.component.html'
})
export class InstructorsDashboardStudentsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>INSTRUCTORS_STUDENTS_LIST_CONFIG;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  private dataSubscription: Subscription;
  instructorID: any = this._activatedRoute.snapshot.params.id;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

    this._activatedTable.activeTable = this.students;

    this.students.query = this._context.model('StudentCounselors')
      .where('instructor').equal(this._activatedRoute.snapshot.params.id)
      .expand('student($expand=person,department)')
      .orderByDescending('fromYear')
      .prepare();

    this.students.config = AdvancedTableConfiguration.cast(INSTRUCTORS_STUDENTS_LIST_CONFIG);

    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.students.fetch(true);
      }
    });

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      if (data.tableConfiguration) {
        this.students.config = data.tableConfiguration;
        this.students.ngOnInit();
      }
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.form.data = {
          instructor: this._activatedRoute.snapshot.params.id
        };
        this.search.ngOnInit();
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
