import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsRootComponent } from './instructors-root.component';

describe('InstructorsRootComponent', () => {
  let component: InstructorsRootComponent;
  let fixture: ComponentFixture<InstructorsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
