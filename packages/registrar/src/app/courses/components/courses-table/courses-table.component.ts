import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {AdvancedTableComponent, AdvancedTableDataResult} from '../../../tables/components/advanced-table/advanced-table.component';
import {AdvancedSearchFormComponent} from '../../../tables/components/advanced-search-form/advanced-search-form.component';
import {UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import * as COURSES_LIST_CONFIG from '../../../courses/components/courses-table/courses-table.config.list.json';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';

@Component({
  selector: 'app-courses-table',
  templateUrl: './courses-table.component.html',
  styles: []
})
export class CoursesTableComponent implements OnInit, OnDestroy {

  public readonly config = COURSES_LIST_CONFIG;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  @Input() department: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  public recordsTotal: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        Object.assign(this.search.form, { department: this._activatedRoute.snapshot.data.department.id });
        /*this.search.formComponent.formLoad.subscribe((res: any) => {
          Object.assign(res, { department: this._activatedRoute.snapshot.data.department.id });
        });*/
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Courses.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
