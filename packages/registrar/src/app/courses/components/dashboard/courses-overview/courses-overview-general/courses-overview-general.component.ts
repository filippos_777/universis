import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-courses-overview-general',
  templateUrl: './courses-overview-general.component.html',
  styleUrls: ['./courses-overview-general.component.scss']
})
export class CoursesOverviewGeneralComponent implements OnInit, OnDestroy {
  public model: any;
  public courseId: any;
  private subscription: Subscription;
  public parentCourse: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseId = params.id;
      this.model = await this._context.model('Courses')
        .where('id').equal(this.courseId)
        .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory,replacedByCourse')
        .getItem();

  if (this.model.courseStructureType.id === 8) {
    this.parentCourse = await this._context.model('Courses')
      .where('id').equal(this.model.parentCourse)
      .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory,replacedByCourse')
      .getItem();
  }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
