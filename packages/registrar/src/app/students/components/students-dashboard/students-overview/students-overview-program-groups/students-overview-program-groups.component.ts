import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { StudentsService } from './../../../../services/students-service/students.service';

@Component({
  selector: 'app-students-overview-program-groups',
  templateUrl: './students-overview-program-groups.component.html'
})
export class StudentsOverviewProgramGroupsComponent implements OnChanges {

  @Input() studentId: number;
  public programGroups: any[];

  constructor(
    private readonly _studentService: StudentsService
  ) { }

  async ngOnChanges() {
    try {
      this.programGroups = await this._studentService.getProgramGroups(this.studentId);
    } catch(err) {
      console.error('err: ', err);
      this.programGroups = [];
    }
  }
}
