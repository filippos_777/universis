import {Component, Input, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {Observable, Subscription} from 'rxjs';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {AppEventService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedFormsService} from '@universis/forms';
import {AngularDataContext} from '@themost/angular';
import {SelectCourseComponent} from '../../../../../courses/components/select-course/select-course-component';
import * as STUDENTS_COURSE_EXEMPTION from './students-courses-exemption-table.config.list.json';
import {AdvancedTableConfiguration} from '../../../../../tables/components/advanced-table/advanced-table.component';
import {AppFilterValueProvider} from '../../../../../registrar-shared/services/app-filter-value-provider.service';

@Component({
  selector: 'app-students-courses-exemption',
  templateUrl: './students-courses-exemption.component.html'
})
export class StudentsCoursesExemptionComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  @Input() execute: Observable<any>;
  public loading: boolean = false;
  public lastError;
  @ViewChild('selectComponent') selectComponent: SelectCourseComponent;
  public formConfig: any;
  public studyProgramCourses;
  @Input() items: Set<any> = new Set();
  @Input() student: any;
  public config: any;
  private selectedItemsSubscription: Subscription;
  @Input() studyProgram: any;

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _formService: AdvancedFormsService,
              private appFilter: AppFilterValueProvider) {
    super(router, activatedRoute);
    this.modalClass = 'modal-lg';
    // this.okButtonDisabled = true;
  }

  async ngOnInit(): Promise<any> {
    this.config = AdvancedTableConfiguration.cast(STUDENTS_COURSE_EXEMPTION);
    if (!this.selectComponent.tableConfig || this.selectComponent.tableConfig.model !== 'StudyProgramCourses') {
      this.selectComponent.ngOnInit(STUDENTS_COURSE_EXEMPTION);
      this.selectComponent.tableConfig.defaults.filter = this.appFilter.buildFilter(this.selectComponent.tableConfig.defaults.filter);
    }
  }

  async selectChanges(event) {
    if(!this.selectedItemsSubscription) {
      this.selectedItemsSubscription = this.selectComponent.advancedTable.selectedItems.subscribe(async (selectedItems) => {
        let courses = this.selectComponent.advancedTable.selected.map(x => {
          return x.course.id;
        });
        if (Array.isArray(selectedItems) && selectedItems.length > 0) {
          for (let item of selectedItems) {
            if (!courses.includes(item.courseID)) {
              await this.fetchCourseData(item);
            }
          }
        } else if (Array.isArray(selectedItems) && selectedItems.length === 0) {
          this.selectComponent.advancedTable.selected = [];
        } else if (typeof selectedItems === 'object') {
          if (!courses.includes(selectedItems.courseID)) {
            await this.fetchCourseData(selectedItems);
          }
        }
      });
      this.selectComponent.advancedTable.selectedItems.next(this.selectComponent.advancedTable.selected[this.selectComponent.advancedTable.selected.length - 1]);
    }
  }

  async fetchCourseData(selectedCourse) {
    let data;
    data = await this._context.model('StudentCourses')
      .where('course')
      .equal(selectedCourse['courseID'])
      .where('student')
      .equal(this.student)
      .expand('course, gradeYear, gradePeriod')
      .getItem();
    if (!data) {
      data = await this._context.model('ProgramCourses')
        .where('course')
        .equal(selectedCourse['courseID'])
        .and('program')
        .equal(this.studyProgram)
        .and('studyProgramSpecialty')
        .equal(selectedCourse['specialization'])
        .expand('course, studyProgramSpecialty')
        .getItem();
    }
    if (Array.isArray(data)) {
      data = data[0];
    }
    if (data && data.calculateUnits === null) {
      data = {...data, 'calculateUnits': true};
    } else {
      data = {...data, 'calculateUnits': data.calculateUnits !== 0};
    }
    if (data && data.calculateGrade === null) {
      data = {...data, 'calculateGrade': true};
    } else {
      data = {...data, 'calculateGrade': data.calculateGrade !== 0};
    }
    data = {...data.course, ...data};
    this.selectComponent.advancedTable.selected[this.selectComponent.advancedTable.selected.length - 1] = {
      ...this.selectComponent.advancedTable.selected[this.selectComponent.advancedTable.selected.length - 1],
      ...data
    };
  }

  ok(): Promise<any> {
    try {
      return new Promise((resolve, reject) => {
        this.loading = false;
        this.lastError = null;
        // assign form attributes
        this.selectComponent.advancedTable.selected.forEach((item) => {
          let studentCourseData = {
            'course': {
              'id': item.courseID,
              'courseArea': item.courseArea,
              'courseCategory': item.courseCategory,
              'courseSector': item.courseSector,
              'name': item.name,
            },
            'displayCode': item.displayCode,
            'ects': item.ects,
            'courseTitle': item.name,
            'courseType': item.courseType,
            'registrationType': 0,
            'semester': item.semester,
            'units': item.units,
            'calculateUnits': item.calculateUnits ? 1 : 0,
            'calculateGrade': item.calculateGrade ? 1 : 0,
            'student': this.student,
            'coefficient': item.coefficient,
            'notes': item.notes,
            'gradeYear': item.gradeYear ? item.gradeYear : null,
            'gradePeriod': item.gradePeriod ? item.gradePeriod : null,
            'specialty': item.specialty,
            'studyProgramSpecialty': item.studyProgramSpecialty
          };
          this.items.add(studentCourseData);
        });
        // execute add
        const executeSubscription = this.execute.subscribe((result) => {
          this.loading = false;
          executeSubscription.unsubscribe();
          // close modal
          if (this._modalService.modalRef) {
            this._modalService.modalRef.hide();
          }
          return resolve();
        }, (err) => {
          this.loading = false;
          // ensure that loading is hidden
          this._loadingService.hideLoading();
          // set last error
          this.lastError = err;
          return resolve();
        });
      });
    } catch (err) {
      this.loading = false;
      this._loadingService.hideLoading();
      this.lastError = err;
    }
  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  close(navigationExtras?: NavigationExtras): Promise<boolean> {
    this.ngOnDestroy();
    return super.close(navigationExtras);
  }

  ngOnDestroy(): void {
    if (this.selectedItemsSubscription) {
      this.selectedItemsSubscription.unsubscribe();
    }
  }

}
