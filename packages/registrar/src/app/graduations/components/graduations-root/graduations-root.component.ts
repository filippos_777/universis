import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-graduations-root',
  templateUrl: './graduations-root.component.html'
})
export class GraduationsRootComponent implements OnInit {

  public model: any;
  public tabs: any[];
  public graduationEventId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.graduationEventId = this._activatedRoute.snapshot.params.id;

    this.model = await this._context.model('GraduationEvents')
      .where('id').equal(this.graduationEventId)
      // .expand('...')
      .getItem();

    this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');

    // save user activity for graduation
    return this._userActivityService.setItem({
      category: this._translateService.instant('Graduations.TitleSingular'),
      description: this._translateService.instant(this.model.name),
      url: window.location.hash.substring(1),
      dateCreated: new Date()
    });
  }

}
