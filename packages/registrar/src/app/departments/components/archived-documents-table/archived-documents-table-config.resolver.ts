import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class ArchivedDocumentsConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
            console.log("resolver:" + route.params);
    return import(`./archived-documents-table.config.${route.params.documentSeries}.json`)
      .catch( err => {
        return  import(`./archived-documents-table.config.json`);
      });
  }
}

export class ArchivedDocumentsConfigurationSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./archived-documents-table.search.${route.params.documentSeries}.json`)
      .catch( err => {
        return  import(`./archived-documents-table.search.json`);
      });
  }
}

export class ArchivedDocumentsDefaultTableConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./archived-documents-table.config.json`);
  }
}
