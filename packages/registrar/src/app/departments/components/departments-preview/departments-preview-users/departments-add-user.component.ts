import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import {
  AdvancedTableModalBaseComponent,
  AdvancedTableModalBaseTemplate,
} from '../../../../tables/components/advanced-table-modal/advanced-table-modal-base.component';
import { AdvancedFilterValueProvider } from '../../../../tables/components/advanced-table/advanced-filter-value-provider.service';

@Component({
  selector: 'app-departments-add-user',
  template: AdvancedTableModalBaseTemplate,
})
export class DepartmentsAddUserComponent extends AdvancedTableModalBaseComponent {
  @Input() department: any;
  public usersToBeInserted = 0;

  constructor(
    protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    protected _context: AngularDataContext,
    private _errorService: ErrorService,
    private _toastService: ToastService,
    private _translateService: TranslateService,
    protected advancedFilterValueProvider: AdvancedFilterValueProvider,
    protected datePipe: DatePipe
  ) {
    super(
      _router,
      _activatedRoute,
      _context,
      advancedFilterValueProvider,
      datePipe
    );
    // set default title
    this.modalTitle = 'Departments.AddUser';
  }

  hasInputs(): Array<string> {
    return ['department'];
  }

  async ok(): Promise<any> {
    const selected = this.advancedTable.selected;
    let items = [];
    if (selected && selected.length > 0) {
      items = selected.map(async (user) => {
        // get user id.
        const selectedUserId = user.userId;
        // get selected user.
        const selectedUser = await this._context
          .model('Users')
          .where('id')
          .equal(selectedUserId)
          .expand('departments')
          .getItem();
        // get user departments.
        const userDepartments = selectedUser.departments;
        // find department index.
        const departmentIndex = userDepartments.findIndex(
          (someDepartment) => someDepartment.id === this.department.id
        );
        // if the department does not already exist, a new user is to be added.
        if (departmentIndex < 0) {
          this.usersToBeInserted++;
        }
        const department = { id: this.department.id };
        return {
          id: user.userId,
          departments: [department]
        };
      });
      // execute promises and get users to be saved.
      const usersToBeSaved = await Promise.all(items);
      // if there are no new users to be inserted, inform and close.
      if (this.usersToBeInserted === 0) {
        this._toastService.show(
          this._translateService.instant('Departments.AddUsersMessage.title'),
          this._translateService.instant(
            'Departments.AddUsersMessage.AllAlreadyExist'
          )
        );
        return this.close();
      }
      // save users.
      return this._context
        .model('Users')
        .save(usersToBeSaved)
        .then(() => {
          this._toastService.show(
            this._translateService.instant('Departments.AddUsersMessage.title'),
            this._translateService.instant(
              this.usersToBeInserted === 1
                ? 'Departments.AddUsersMessage.one'
                : 'Departments.AddUsersMessage.many',
              { value: this.usersToBeInserted }
            )
          );
          return this.close({
            fragment: 'reload',
            skipLocationChange: true,
          });
        })
        .catch((err) => {
          this._errorService.showError(err, {
            continueLink: '.',
          });
        });
    }
    return this.close();
  }
}
