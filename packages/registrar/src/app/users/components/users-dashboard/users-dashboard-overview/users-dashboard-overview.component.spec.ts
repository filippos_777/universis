import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersDashboardOverviewComponent } from './users-dashboard-overview.component';

describe('UsersDashboardOverviewComponent', () => {
  let component: UsersDashboardOverviewComponent;
  let fixture: ComponentFixture<UsersDashboardOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersDashboardOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersDashboardOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
